package com.icheung.lessthanthree.fragment.data;


import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.icheung.lessthanthree.helper.PhotoAdapter;
import com.icheung.lessthanthree.retrofit.model.Data;

import java.util.ArrayList;
import java.util.List;

public class PhotoDataFragment extends Fragment {
    private PhotoAdapter adapter;
    private List<Data> data;

    public PhotoDataFragment(){
        data = new ArrayList<Data>();
        adapter = new PhotoAdapter(data);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    public void setAdapter(PhotoAdapter adapter) {
        this.adapter = adapter;
    }

    public PhotoAdapter getAdapter() {
        return adapter;
    }

    public void setData(ArrayList<Data> data) {
        this.data = data;
    }

    public List<Data> getData() {
        return data;
    }
}
