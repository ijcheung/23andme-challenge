package com.icheung.lessthanthree;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.icheung.lessthanthree.fragment.data.PhotoDataFragment;
import com.icheung.lessthanthree.helper.InfiniteScrollListener;
import com.icheung.lessthanthree.helper.PhotoAdapter;
import com.icheung.lessthanthree.retrofit.InstagramApi;
import com.icheung.lessthanthree.retrofit.InstagramApiGenerator;
import com.icheung.lessthanthree.retrofit.model.Data;
import com.icheung.lessthanthree.retrofit.model.Envelope;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,
        AdapterView.OnItemSelectedListener {
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final String TAG_PHOTO_DATA_FRAGMENT = "photo_data_fragment";
    private static final String STATE_MODE = "mode";
    private static final String REDIRECT_HOST = "www.23andme.com";

    private static final int SEARCH_RECENT = 0;
    private static final int SEARCH_LOCATION = 1;
    private static final int SEARCH_TAG = 2;

    private RecyclerView mRecyclerView;
    private View mOnboarding;

    private View mSearchBar;
    private Spinner mSearchSpinner;
    private EditText mSearchTerm;
    private View mSearchButton;

    private Dialog mDialog;

    private MenuItem mLogin;
    private MenuItem mLogout;

    private String mToken;
    private InstagramApi mApi;
    private Geocoder mGeocoder;

    private HandlerThread mBackgroundThread;
    private Handler mBackgroundHandler;

    private PhotoDataFragment mPhotoDataFragment;
    private PhotoAdapter mAdapter;
    private List<Data> mPhotos;
    private InfiniteScrollListener mInfiniteScrollListener;

    private double mLatitude;
    private double mLongitude;
    private int mMode = SEARCH_RECENT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mOnboarding = findViewById(R.id.onboarding);
        mSearchBar = findViewById(R.id.search_bar);
        mSearchSpinner = (Spinner) findViewById(R.id.search_spinner);
        mSearchTerm = (EditText) findViewById(R.id.search_term);
        mSearchButton = findViewById(R.id.search_button);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mApi = InstagramApiGenerator.generate();
        mGeocoder = new Geocoder(this, Locale.getDefault());

        mBackgroundThread = new HandlerThread("background");
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper());

        // Find Retained Data Fragment
        FragmentManager fm = getSupportFragmentManager();
        mPhotoDataFragment = (PhotoDataFragment) fm.findFragmentByTag(TAG_PHOTO_DATA_FRAGMENT);

        // Create Data Fragment
        if (mPhotoDataFragment == null) {
            mPhotoDataFragment = new PhotoDataFragment();
            fm.beginTransaction().add(mPhotoDataFragment, TAG_PHOTO_DATA_FRAGMENT).commit();
        }

        mPhotos = mPhotoDataFragment.getData();
        mAdapter = mPhotoDataFragment.getAdapter();

        GridLayoutManager layoutManager = new GridLayoutManager(this,
                getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT
                        ? 1 : 2);

        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapter);

        mInfiniteScrollListener = new InfiniteScrollListener(layoutManager) {
            @Override
            public void onLoadMore() {
                loadPhotos();
            }
        };

        mSearchSpinner.setAdapter(new SearchTypeAdapter(this));
        mSearchSpinner.setOnItemSelectedListener(this);
        mSearchButton.setOnClickListener(this);
        // http://stackoverflow.com/questions/1489852/android-handle-enter-in-an-edittext
        mSearchTerm.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(event.getAction() == KeyEvent.ACTION_DOWN) {
                    doSearch();
                    return true;
                }
                return false;
            }
        });

        mToken = getPreferences(Context.MODE_PRIVATE).getString(getString(R.string.pref_token), null);
        if (mToken != null) {
            // Already have token
            mAdapter.setToken(mToken);
            loadPhotos();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);

        mLogin = menu.findItem(R.id.action_login);
        mLogout = menu.findItem(R.id.action_logout);
        updateLoginState();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_login:
                startOAuthFlow();
                return true;
            case R.id.action_logout:
                mToken = null;

                // Clear login on web view
                // http://stackoverflow.com/questions/28998241/how-to-clear-cookies-and-cache-of-webview-on-android-when-not-in-webview
                CookieManager.getInstance().removeAllCookies(null);
                CookieManager.getInstance().flush();

                // Clear shared pref
                SharedPreferences.Editor e = getPreferences(Context.MODE_PRIVATE).edit();
                e.putString(getString(R.string.pref_token), null);
                e.apply();

                // Clear loaded photos
                mAdapter.reset();

                // Reset search bar state
                mSearchSpinner.setSelection(0);
                mMode = SEARCH_RECENT;
                mSearchTerm.setVisibility(View.INVISIBLE);
                mSearchButton.setVisibility(View.INVISIBLE);

                updateLoginState();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putInt(STATE_MODE, mMode);

        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        mMode = savedInstanceState.getInt(STATE_MODE);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.search_button:
                doSearch();
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch(position) {
            case SEARCH_RECENT:
                mSearchTerm.setVisibility(View.INVISIBLE);
                mSearchButton.setVisibility(View.INVISIBLE);
                if(mMode != SEARCH_RECENT) {
                    mMode = SEARCH_RECENT;
                    mAdapter.reset();
                    loadPhotos();
                }
                break;
            case SEARCH_LOCATION:
                mSearchTerm.setText("");
                mSearchTerm.setHint(R.string.location);
                mSearchTerm.setVisibility(View.VISIBLE);
                mSearchButton.setVisibility(View.VISIBLE);
                break;
            case SEARCH_TAG:
                mSearchTerm.setText("");
                mSearchTerm.setHint(R.string.tag);
                mSearchTerm.setVisibility(View.VISIBLE);
                mSearchButton.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void updateLoginState() {
        boolean loggedIn = mToken != null;
        mLogin.setVisible(!loggedIn);
        mLogout.setVisible(loggedIn);
        mSearchBar.setVisibility(loggedIn ? View.VISIBLE : View.GONE);
        mRecyclerView.setVisibility(loggedIn ? View.VISIBLE : View.INVISIBLE);
        mOnboarding.setVisibility(loggedIn ? View.INVISIBLE : View.VISIBLE);
    }

    private void loadPhotos() {
        Log.d(TAG, "loadPhotos: " + mAdapter.getNextMaxId());

        if(mAdapter.getNextMaxId() != null) {
            Call<Envelope> call;
            switch(mMode) {
                case SEARCH_RECENT:
                    call = mApi.getRecentImages(mToken, mAdapter.getNextMaxId());
                    break;
                case SEARCH_LOCATION:
                    call = mApi.getImagesByLocation(mLatitude, mLongitude, mToken);
                    break;
                case SEARCH_TAG:
                    call = mApi.getRecentImagesByTag(mSearchTerm.getText().toString().replaceAll("[^0-9a-zA-Z ]", ""), mToken, mAdapter.getMinTagId());
                    break;
                default:
                    Log.e(TAG, "loadPhotos invalid mode: " + mMode);
                    return;
            }

            call.enqueue(new Callback<Envelope>() {
                @Override
                public void onResponse(Call<Envelope> call, retrofit2.Response<Envelope> response) {
                    switch (response.code()) {
                        case 200:
                            Envelope envelope = (response.body());
                            mAdapter.loadPage(envelope);
                            if(envelope.getData().size() == 0) {
                                Toast.makeText(MainActivity.this, R.string.error_no_photos, Toast.LENGTH_SHORT).show();
                            }
                            break;
                        default:
                            Log.d(TAG, "loadPhotos error: " + response.code());
                            mInfiniteScrollListener.onLoadFailed();
                    }
                }

                @Override
                public void onFailure(Call<Envelope> call, Throwable t) {
                    Log.e(TAG, "loadPhotos onFailure: " + t.getMessage());
                    mInfiniteScrollListener.onLoadFailed();
                    Toast.makeText(MainActivity.this, R.string.error_connection, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void doSearch() {
        if(TextUtils.isEmpty(mSearchTerm.getText())) {
            Toast.makeText(this, R.string.error_empty_search_term, Toast.LENGTH_SHORT).show();
        } else {
            mMode = mSearchSpinner.getSelectedItemPosition();
            mAdapter.reset();

            switch(mMode) {
                case SEARCH_LOCATION:
                    geocodeAndSearch();
                    break;
                case SEARCH_TAG:
                    loadPhotos();
            }
        }
    }

    private void geocodeAndSearch() {
        mBackgroundHandler.post(new Runnable() {
            @Override
            public void run() {
                try {
                    List<Address> results = mGeocoder.getFromLocationName(mSearchTerm.getText().toString(), 1);
                    if(results.size() == 1) {
                        Address address = results.get(0);
                        mLatitude = address.getLatitude();
                        mLongitude = address.getLongitude();
                        loadPhotos();
                    } else {
                        Toast.makeText(MainActivity.this, R.string.error_no_geocoder_results, Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException e) {
                    Log.e(TAG, "Geocoder Fail", e);
                    Toast.makeText(MainActivity.this, R.string.error_geocoder, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    // OAuth 2.0 WebView intercept
    // http://techblog.constantcontact.com/software-development/implementing-oauth2-0-in-an-android-app/
    private void startOAuthFlow() {
        LayoutInflater inflater = getLayoutInflater();
        WebView webView = (WebView) inflater.inflate(R.layout.web_view, null);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return handleUrl(view, Uri.parse(url));
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest resource) {
                return handleUrl(view, resource.getUrl());
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(this, android.R.style.Theme_DeviceDefault_Light_Dialog);
        builder.setView(webView);
        mDialog = builder.create();
        mDialog.show();

        // Do OAuth 2.0 login
        webView.loadUrl("https://api.instagram.com/oauth/authorize/?client_id=" + BuildConfig.INSTAGRAM_CLIENT_ID + "&redirect_uri=https://www.23andme.com&response_type=token&scope=likes+public_content");
    }

    private boolean handleUrl(WebView view, Uri url) {
        Log.i(TAG, "host=" + url.getHost());
        if(url.getHost().startsWith(REDIRECT_HOST)) {
            // Extract mToken
            String fragment = url.getFragment();
            Log.i(TAG, "fragment=" + fragment);

            if(TextUtils.isEmpty(fragment) || !fragment.startsWith("access_token")) {
                Log.e(TAG, "redirect with no access mToken");
            } else {
                mToken = fragment.substring(13);
                mAdapter.setToken(mToken);
                Log.i(TAG, "token=" + mToken);

                // store in default SharedPreferences
                SharedPreferences.Editor e = getPreferences(Context.MODE_PRIVATE).edit();
                e.putString(getString(R.string.pref_token), mToken);
                e.apply();

                updateLoginState();
                loadPhotos();
            }

            mDialog.cancel();
            // don't go to redirectUri
            return true;
        }

        return false;
    }

    static class SearchTypeAdapter extends ArrayAdapter<String> {
        private static final int[] sIcons = {R.drawable.ic_recent, R.drawable.ic_location, R.drawable.ic_tag};
        private static final int[] sTitles = {R.string.recent, R.string.location, R.string.tag};

        private LayoutInflater mInflater;

        public SearchTypeAdapter(Context context) {
            super(context, 0);

            mInflater = LayoutInflater.from(context);
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            View view;

            if(convertView == null) {
                view = mInflater.inflate(R.layout.list_item, parent, false);
            } else {
                view = convertView;
            }

            ImageView icon = (ImageView) view.findViewById(R.id.icon);
            TextView label=(TextView) view.findViewById(R.id.title);
            icon.setImageResource(sIcons[position]);
            label.setText(sTitles[position]);

            return view;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;

            if(convertView == null) {
                view = mInflater.inflate(R.layout.icon_item, parent, false);
            } else {
                view = convertView;
            }

            ImageView icon = (ImageView) view.findViewById(R.id.icon);
            icon.setImageResource(sIcons[position]);

            return view;
        }

        @Override
        public int getCount() {
            return sTitles.length;
        }
    }
}
