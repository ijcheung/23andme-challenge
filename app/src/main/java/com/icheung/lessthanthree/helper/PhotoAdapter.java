package com.icheung.lessthanthree.helper;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.icheung.lessthanthree.R;
import com.icheung.lessthanthree.databinding.PhotoItemBinding;
import com.icheung.lessthanthree.retrofit.InstagramApi;
import com.icheung.lessthanthree.retrofit.InstagramApiGenerator;
import com.icheung.lessthanthree.retrofit.model.Data;
import com.icheung.lessthanthree.retrofit.model.Envelope;
import com.icheung.lessthanthree.retrofit.model.Image;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.ViewHolder> {
    private static final String TAG = PhotoAdapter.class.getSimpleName();

    private final List<Data> mPhotos;
    private String mNextMaxId = "0";
    private String mMinTagId = "0";
    private String mToken;
    private InstagramApi mApi;

    public PhotoAdapter(List<Data> photos){
        this.mPhotos = photos;
        mApi = InstagramApiGenerator.generate();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        return new ViewHolder((PhotoItemBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.photo_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        PhotoItemBinding binding = holder.getBinding();
        Data data = mPhotos.get(position);
        binding.setData(data);
        binding.executePendingBindings();
        Image image = data.getImages().getStandardImage();
        Glide.with(binding.photo.getContext())
                .load(image.getUrl())
                .placeholder(R.drawable.ic_image)
                .error(R.drawable.ic_image_broken)
                .into(binding.photo);
    }

    @Override
    public int getItemCount() {
        return mPhotos.size();
    }

    public String getNextMaxId() {
        return mNextMaxId;
    }

    public String getMinTagId() {
        return  mMinTagId;
    }

    public void setToken(String token) {
        mToken = token;
    }

    public void loadPage(Envelope envelope) {
        mNextMaxId = envelope.getPagination() == null ? null : envelope.getPagination().getNextMaxId();
        mMinTagId = envelope.getPagination() == null ? null : envelope.getPagination().getMinTagId();
        mPhotos.addAll(envelope.getData());
        notifyItemRangeInserted(mPhotos.size() - envelope.getData().size(), envelope.getData().size());
    }

    public void reset() {
        mPhotos.clear();
        mNextMaxId = "0";
        mMinTagId = "0";
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private final PhotoItemBinding binding;

        public ViewHolder(PhotoItemBinding itemBinding) {
            super(itemBinding.getRoot());
            binding = itemBinding;
            binding.heart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Data data = binding.getData();
                    boolean liked = data.isLiked();
                    binding.heart.setImageResource(liked ? R.drawable.ic_heart_outline : R.drawable.ic_heart);
                    data.setLiked(!liked);
                    Call<Envelope> call = liked
                            ? mApi.removeLike(data.getId(), mToken)
                            : mApi.addLike(data.getId(), mToken);
                    call.enqueue(new Callback<Envelope>(){
                        @Override
                        public void onResponse(Call<Envelope> call, Response<Envelope> response) {
                            switch (response.code()) {
                                case 200:
                                    break;
                                default:
                                    Log.d(TAG, "changeLike error: " + response.code());
                                    Toast.makeText(binding.getRoot().getContext(), R.string.error_connection, Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<Envelope> call, Throwable t) {
                            Log.e(TAG, "changeLike error: " + t.getMessage());
                            Toast.makeText(binding.getRoot().getContext(), R.string.error_connection, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            });
        }

        public PhotoItemBinding getBinding() {
            return binding;
        }
    }
}
