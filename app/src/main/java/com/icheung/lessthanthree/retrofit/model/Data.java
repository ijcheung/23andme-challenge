package com.icheung.lessthanthree.retrofit.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Data implements Parcelable {
    private String id;
    private Images images;
    @SerializedName("user_has_liked")
    private boolean isLiked;

    public String getId() {
        return id;
    }

    public Images getImages() {
        return images;
    }

    public boolean isLiked() {
        return isLiked;
    }

    public void setLiked(boolean liked) {
        isLiked = liked;
    }

    protected Data(Parcel in) {
        id = in.readString();
        images = (Images) in.readValue(Images.class.getClassLoader());
        isLiked = in.readByte() != 0x00;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeValue(images);
        dest.writeByte((byte) (isLiked ? 0x01 : 0x00));
    }

    @SuppressWarnings("unused")
    public static final Creator<Data> CREATOR = new Creator<Data>() {
        @Override
        public Data createFromParcel(Parcel in) {
            return new Data(in);
        }

        @Override
        public Data[] newArray(int size) {
            return new Data[size];
        }
    };
}