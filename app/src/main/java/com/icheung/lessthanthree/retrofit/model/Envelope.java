package com.icheung.lessthanthree.retrofit.model;

import java.util.List;

public class Envelope {
    private Meta meta;
    private List<Data> data;
    private Pagination pagination;

    public Meta getMeta() {
        return meta;
    }

    public List<Data> getData() {
        return data;
    }

    public Pagination getPagination() {
        return pagination;
    }
}
