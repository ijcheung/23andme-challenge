package com.icheung.lessthanthree.retrofit.model;

import com.google.gson.annotations.SerializedName;

public class Pagination {
    @SerializedName("next_max_id")
    private String nextMaxId;

    @SerializedName("min_tag_id")
    private String minTagId;

    public String getNextMaxId() {
        return nextMaxId;
    }

    public String getMinTagId() {
        return minTagId;
    }
}
