package com.icheung.lessthanthree.retrofit.model;

public class Meta {
    private int code;
    private String errorType;
    private String errorMessage;

    public int getCode() {
        return code;
    }

    public String getErrorType() {
        return errorType;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}