package com.icheung.lessthanthree.retrofit.model;

import com.google.gson.annotations.SerializedName;

public class AccessToken {
    private int code;
    @SerializedName("access_token")
    private String accessToken;

    public int getCode() {
        return code;
    }

    public String getAccessToken() {
        return accessToken;
    }
}