package com.icheung.lessthanthree.retrofit.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Images implements Parcelable {
    @SerializedName("low_resolution")
    private Image lowResImage;
    private Image thumbnail;
    @SerializedName("standard_resolution")
    private Image standardImage;

    public Image getLowResImage() {
        return lowResImage;
    }

    public Image getThumbnail() {
        return thumbnail;
    }

    public Image getStandardImage() {
        return standardImage;
    }

    protected Images(Parcel in) {
        lowResImage = (Image) in.readValue(Image.class.getClassLoader());
        thumbnail = (Image) in.readValue(Image.class.getClassLoader());
        standardImage = (Image) in.readValue(Image.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(lowResImage);
        dest.writeValue(thumbnail);
        dest.writeValue(standardImage);
    }

    @SuppressWarnings("unused")
    public static final Creator<Images> CREATOR = new Creator<Images>() {
        @Override
        public Images createFromParcel(Parcel in) {
            return new Images(in);
        }

        @Override
        public Images[] newArray(int size) {
            return new Images[size];
        }
    };
}