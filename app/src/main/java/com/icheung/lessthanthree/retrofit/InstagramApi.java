package com.icheung.lessthanthree.retrofit;

import com.icheung.lessthanthree.retrofit.model.Envelope;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface InstagramApi {
    String BASE_URL = "https://api.instagram.com/";

    @GET("v1/users/self/media/recent/")
    Call<Envelope> getRecentImages(
            @Query("access_token") String token,
            @Query("max_id") String maxId
    );

    @GET("v1/media/search")
    Call<Envelope> getImagesByLocation(
            @Query("lat") double latitude,
            @Query("lng") double longitude,
            @Query("access_token") String token
    );

    @GET("v1/tags/{tag-name}/media/recent")
    Call<Envelope> getRecentImagesByTag(
            @Path("tag-name") String tag,
            @Query("access_token") String token,
            @Query("min_tag_id") String minTagId
    );

    @FormUrlEncoded
    @POST("v1/media/{media-id}/likes")
    Call<Envelope> addLike(
            @Path("media-id") String mediaId,
            @Field("access_token") String token
    );

    @DELETE("v1/media/{media-id}/likes")
    Call<Envelope> removeLike(
            @Path("media-id") String mediaId,
            @Query("access_token") String token
    );
}