# <3 lessthanthree

## Getting Started

This project uses the Gradle build system.  To build this project, use the
"gradlew build" command or use "Import Project" in Android Studio.

### Instagram API Client ID is required.

In order for the app to function properly, an API Client ID for Instagram must be included with the build.

[Generate an API key](https://www.instagram.com/developer/), and include the client id for the build by adding the following lines to [USER_HOME]/.gradle/gradle.properties

    InstagramClientId="???"
	
## Libraries Used

### [Design Support Library](https://developer.android.com/training/material/design-library.html)

The design support library is used to achieve the scrolling app bar behavior. While this is a rather simple behavior to create, the design library makes it very easy to implement albeit at the potential cost of loss of access to some newer APIs.

### [Glide](https://github.com/bumptech/glide)

Glide is used for asynchronous image loading and caching. Glide is considered to have a lower memory footprint than its counterparts at the cost of lower default image quality. This means that it is not a good idea to use it for zoom views out of the box.

### [Retrofit](http://square.github.io/retrofit/)

Retrofit is used to create a Java interface for calling the Instagram API. It handles doing the calls asynchronously as well as serializing and de-serializing between Java and JSON.

Retrofit is designed specifically for consumption of APIs and as a result is higher level with cleaner and simpler code than lower level alternatives like OkHTTP or Volley.

**converter-gson** is used for the SerializedName annotation, which allows the POJO to have different variable names than its corresponding JSON.

**logging-interceptor** is used for debug purposes so that the request and responses can be examined within logcat.
